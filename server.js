const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const passport = require("passport");

const usersRoute = require("./routes/api/users");
const todosRoute = require("./routes/api/todos");

const app = express();

// Bodyparser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// DB config
const db = require("./config/keys").mongoURI;

// Connect to MongoDB
mongoose
	.connect(db, {
		useNewUrlParser: true,
		useFindAndModify: false,
		useCreateIndex: true
	})
	.then(() => console.log("MongoDB successfully connected"))
	.catch(err => console.log(err));

// Passport middleware
app.use(passport.initialize());

// Passport config
require("./config/passport")(passport);

// Routes
app.use("/api/users", usersRoute);
app.use("/api/todos", todosRoute);

const port = process.env.PORT || 5000;

if (process.env.NODE_ENV === "production") {
	const path = require("path");
	app.use(express.static(path.resolve(__dirname, "./frontend/build")));
	app.get("*", (req, res) => {
		res.sendFile(path.resolve(__dirname, "./frontend/build", "index.html"));
	});
}

app.listen(port, () => {
	console.log(`Server up and running on port ${port}`);
});
