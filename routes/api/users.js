const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const keys = require("../../config/keys");

const validateRegisterInput = require("../../validation/register");
const validateLoginInput = require("../../validation/login");

// TODO: querying documents with no intent to modify them is more performant using 'lean' option on query

const User = require("../../models/User");

// @route POST api/users/register
// @desc Register user
// @access Public
router.post("/register", (req, res) => {
	let { errors, isValid } = validateRegisterInput(req.body);

	if (!isValid) {
		return res.status(400).json(errors);
	}

	let { name, email, password } = req.body;

	User.findOne({ email: email }).then(user => {
		if (user) {
			return res.status(400).json({ email: "Email already exists" });
		} else {
			let newUser = new User({
				name: name,
				email: email,
				password: password
			});

			// Hash password before saving in database
			// TODO: create a 'pre' middleware function for password hashing using plugin method
			bcrypt.genSalt(10, (err, salt) => {
				if (err) throw err;
				bcrypt.hash(newUser.password, salt, (err, hash) => {
					if (err) throw err;
					newUser.password = hash;
					newUser
						.save()
						.then(user => res.json(user))
						.catch(err => console.log(err));
				});
			});
		}
	});
});

// @route POST api/users/login
// @desc Login user and return JWT token
// @access Public
router.post("/login", (req, res) => {
	let { errors, isValid } = validateLoginInput(req.body);

	if (!isValid) {
		return res.status(400).json(errors);
	}

	let { email, password } = req.body;

	User.findOne({ email }).then(user => {
		if (!user) {
			return res.status(404).json({ emailnotfound: "Email not found" });
		}

		// Check password
		bcrypt.compare(password, user.password).then(isMatch => {
			if (isMatch) {
				// User matched
				// Create JWT payload
				let payload = {
					id: user.id,
					name: user.name
				};

				// Sign token
				jwt.sign(
					payload,
					keys.secretOrKey,
					{
						expiresIn: 31556926 // 1 year in seconds
					},
					(err, token) => {
						res.json({
							success: true,
							token: "Bearer " + token
						});
					}
				);
			} else {
				return res
					.status(400)
					.json({ passwordincorrect: "Password incorrect" });
			}
		});
	});
});

// @route POST /api/users/find
// @desc Get user data
// @access Public
router.post("/find", (req, res) => { 
	let { email } = req.body;

	if (!email) {
		return res.status(400).json({ emailnotprovided: "Email was not provided" });
	}

	User.findOne({ email: email }).then(user => {
		if (!user) {
			return res.status(400).json({ userwasnotfound: "User was not found" });
		} else {
			return res.status(400).json({ username: user.name });
		}
	});
});

module.exports = router;
