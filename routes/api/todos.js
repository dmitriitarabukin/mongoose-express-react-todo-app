const express = require("express");
const router = express.Router();

const User = require("../../models/User");
const Todo = require("../../models/Todo");

// @route api/todos/add
// @desc Add todo-item
// @access Public
router.post("/add", (req, res) => {
	let { user_id, text } = req.body;

	User.findById(user_id).then(user => {
		if (!user) {
			return res.status(400).json({ usernotfound: "User was not found" });
		} else {
			let todo = new Todo({
				user: user_id,
				text: text
			});

			todo
				.save()
				.then(todo => res.json(todo))
				.catch(err => console.log(err));
		}
	});
});

// @route api/todos/get
// @desc Get all user's todos
// @access Public
router.post("/get", (req, res) => {
	let { user_id } = req.body;

	Todo.find({ user: user_id }, "-user -__v").then(todos => {
		if (!todos) {
			return res
				.status(400)
				.json({ todosnotfound: "Todo items were not found" });
		} else {
			res.json(todos);
		}
	});
});

// @route api/todos/update
// @desc Update user's todo item
// @access Public
router.put("/update", (req, res) => {
	let { todo_id, new_text } = req.body;

	Todo.findOneAndUpdate(
		{ _id: todo_id },
		{ text: new_text },
		{ new: true }
	).then(todo => {
		if (!todo) {
			return res
				.status(400)
				.json({ failedtoupdatetodo: "Failed updating todo item" });
		} else {
			res.json(todo);
		}
	});
});

// @route api/todos/delete
// @desc Delete user's todo item
// @access Public
router.delete("/delete", (req, res) => {
	let { todo_id } = req.body;

	Todo.findOneAndDelete({ _id: todo_id }).then(todo => {
		if (!todo) {
			return res
				.status(400)
				.json({ failedtodeletetodo: "Failed deleting todo item" });
		} else {
			res.json(todo);
		}
	});
});

module.exports = router;
