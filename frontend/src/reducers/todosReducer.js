import {
	SET_TODOS,
	ADD_TODO,
	DELETE_TODO,
	UPDATE_TODO,
	TODO_LOADING
} from "../actions/types";

const initialState = {
	todos: [],
	is_loading: false
};

export default function(state = initialState, action) {
	switch (action.type) {
		case SET_TODOS:
			return {
				...state,
				todos: action.payload
			};
		case ADD_TODO:
			return {
				...state,
				todos: [...state.todos, action.payload]
			};
		case UPDATE_TODO:
			return {
				...state,
				todos: state.todos.map(todo => {
					if (todo.id === action.payload.id) {
						todo.text = action.payload.text;
					}
					return todo;
				})
			};
		case DELETE_TODO:
			return {
				...state,
				todos: state.todos.filter(todo => todo._id !== action.payload)
			};
		case TODO_LOADING:
			return {
				...state,
				is_loading: action.payload
			};
		default:
			return state;
	}
}
