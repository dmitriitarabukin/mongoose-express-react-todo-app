import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import TodoItem from "./TodoItem";
import {
	getUserTodos,
	addTodo,
	deleteTodo,
	updateTodo
} from "../../actions/todosActions";

class TodoList extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			new_todo_text: ""
		};

		this.setnew_todo_text = this.setnew_todo_text.bind(this);
		this.updateTodo = this.updateTodo.bind(this);
		this.addTodo = this.addTodo.bind(this);
		this.deleteTodo = this.deleteTodo.bind(this);
	}

	componentDidMount() {
		this.props.getUserTodos({ user_id: this.props.userId });
	}

	setnew_todo_text(event) {
		this.setState({
			new_todo_text: event.target.value
		});
	}

	updateTodo(todo_id, new_text) {
		this.props.updateTodo({ todo_id, new_text });
	}

	addTodo() {
		this.props.addTodo({
			user_id: this.props.userId,
			text: this.state.new_todo_text
		});
	}

	deleteTodo(todo_id) {
		this.props.deleteTodo({ todo_id });
	}

	render() {
		let todos = this.props.todos_data.todos.map(todo => (
				<TodoItem
					key={todo._id}
					id={todo._id}
					text={todo.text}
					updateTodo={this.updateTodo}
					deleteTodo={this.deleteTodo}
				/>
			)),
			{ new_todo_text } = this.state,
			is_new_todo_loading = this.props.todos_data.is_loading;

		return (
			<div>
				<React.Fragment>
					{todos.length > 0 && <ul>{todos}</ul>}
					<div className="row">
						<div className="input-field col s9">
							<input
								type="text"
								onChange={this.setnew_todo_text}
								value={new_todo_text}
							/>
						</div>
						<div
							style={{ marginTop: "10px" }}
							className="col s12 m3 valign-wrapper"
						>
							<button
								className="btn-floating waves-effect waves-light btn-large blue"
								onClick={this.addTodo}
								style={{ marginRight: "15px" }}
							>
								Add
							</button>
							<div
								className={
									"preloader-wrapper small " +
									(is_new_todo_loading ? "active" : "")
								}
							>
								<div className="spinner-layer spinner-blue-only">
									<div className="circle-clipper left">
										<div className="circle" />
									</div>
									<div className="gap-patch">
										<div className="circle" />
									</div>
									<div className="circle-clipper right">
										<div className="circle" />
									</div>
								</div>
							</div>
						</div>
					</div>
				</React.Fragment>
			</div>
		);
	}
}

TodoList.propTypes = {
	getUserTodos: PropTypes.func.isRequired,
	addTodo: PropTypes.func.isRequired,
	deleteTodo: PropTypes.func.isRequired,
	updateTodo: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
	todos_data: state.todos
});

export default connect(
	mapStateToProps,
	{ getUserTodos, addTodo, deleteTodo, updateTodo }
)(TodoList);
