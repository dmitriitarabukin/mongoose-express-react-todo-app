import React from "react";

class TodoItem extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isEdit: false,
			text: props.text
		};
		this.toggleEditState = this.toggleEditState.bind(this);
		this.setText = this.setText.bind(this);
		this.confirmNewText = this.confirmNewText.bind(this);
	}

	toggleEditState() {
		this.setState({
			isEdit: !this.state.isEdit
		});		
	}

	setText(event) {
		this.setState({
			text: event.target.value
		});
	}

	confirmNewText() {
		let { updateTodo, id } = this.props,
			{ text } = this.state;

		updateTodo(id, text);
		this.toggleEditState();
	}

	render() {
		let { text } = this.state,
			{ deleteTodo, id } = this.props;

		return (
			<li>
				<div className="row">
					{this.state.isEdit ? (
						<React.Fragment>
							<div className="input-field col s12 l9">
								<input
									type="text"
									onChange={this.setText}
									value={text}
								/>
							</div>
							<div className="col s12 l3 right-align">
								<button
									className="waves-effect waves-light btn-small blue"
									onClick={this.confirmNewText}
								>
									Confirm
								</button>
								<button
									className="waves-effect waves-light btn-small red"
									onClick={() => deleteTodo(id)}
									style={{ marginLeft: "10px" }}
								>
									Remove
								</button>
							</div>
						</React.Fragment>
					) : (
						<React.Fragment>
							<div className="col s12 m9">
								<span>{text}</span>
							</div>
							<div className="col l3 s12 right-align">
								<button
									className="waves-effect waves-light btn-small"
									onClick={this.toggleEditState}
								>
									Edit
								</button>
								<button
									className="waves-effect waves-light btn-small red"
									onClick={() => deleteTodo(id)}
									style={{ marginLeft: "10px" }}
								>
									Remove
								</button>
							</div>
						</React.Fragment>
					)}
				</div>
			</li>
		);
	}
}

export default TodoItem;
