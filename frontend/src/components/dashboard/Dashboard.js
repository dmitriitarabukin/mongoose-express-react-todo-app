import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { logoutUser } from "../../actions/authActions";
import TodoList from "./TodoList";

class Dashboard extends Component {
  onLogoutClick = e => {
    e.preventDefault();
    this.props.logoutUser();
  };

  render() {
    let { user } = this.props.auth,
      username = user.name.split(" ")[0];
      
    return (
      <React.Fragment>
        <div className="container">
          <div className="row">
            <div className="col s12">
              <TodoList userId={user.id} />
            </div>
          </div>
        </div>
        <div
          style={{
            position: "fixed",
            bottom: "20px",
            right: "20px",
            textAlign: "center"
          }}
        >
          <span
            style={{
              display: "block",
              fontSize: "0.8rem",
              marginBottom: "5px"
            }}
          >
            <b>Hi,</b> {username}!
          </span>
          <button
            style={{
              width: "100px",
              borderRadius: "3px",
              letterSpacing: "1.5px"
            }}
            onClick={this.onLogoutClick}
            className="btn btn-small waves-effect waves-light hoverable blue accent-3"
          >
            Logout
          </button>
        </div>
      </React.Fragment>
    );
  }
}

Dashboard.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { logoutUser }
)(Dashboard);
