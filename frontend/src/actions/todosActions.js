import axios from "axios";

import {
	GET_ERRORS,
	SET_TODOS,
	ADD_TODO,
	DELETE_TODO,
	UPDATE_TODO,
	TODO_LOADING
} from "./types";

export const getUserTodos = user_data => dispatch => {
	dispatch(setTodoLoadingAction(true));
	axios
		.post("/api/todos/get", user_data)
		.then(res => {
			dispatch(setUserTodosAction(res.data));
			dispatch(setTodoLoadingAction(false));
		})
		.catch(err => {
			dispatch({
				type: GET_ERRORS,
				payload: err.response.data
			});
			dispatch(setTodoLoadingAction(false));
		});
};

export const addTodo = data => dispatch => {
	dispatch(setTodoLoadingAction(true));
	axios
		.post("/api/todos/add", data)
		.then(res => {
			dispatch(addTodoAction(res.data));
			dispatch(setTodoLoadingAction(false));
		})
		.catch(err => {
			dispatch({
				type: GET_ERRORS,
				payload: err.response.data
			});
			dispatch(setTodoLoadingAction(false));
		});
};

export const updateTodo = data => dispatch => {
	dispatch(setTodoLoadingAction(true));
	axios
		.put("/api/todos/update", data)
		.then(res => {
			dispatch(updateTodoAction(res.data));
			dispatch(setTodoLoadingAction(false));
		})
		.catch(err => {
			dispatch({
				type: GET_ERRORS,
				payload: err.response.data
			});
			dispatch(setTodoLoadingAction(false));
		});
};

export const deleteTodo = data => dispatch => {
	dispatch(setTodoLoadingAction(true));
	axios
		.delete("/api/todos/delete", { data })
		.then(res => {
			dispatch(deleteTodoAction(res.data._id));
			dispatch(setTodoLoadingAction(false));
		})
		.catch(err => {
			dispatch({
				type: GET_ERRORS,
				payload: err.response.data
			});
			dispatch(setTodoLoadingAction(false));
		});
};

export const setUserTodosAction = todos => {
	return {
		type: SET_TODOS,
		payload: todos
	};
};

export const addTodoAction = todo => {
	return {
		type: ADD_TODO,
		payload: todo
	};
};

export const updateTodoAction = todo => {
	return {
		type: UPDATE_TODO,
		payload: todo
	};
};

export const deleteTodoAction = todo_id => {
	return {
		type: DELETE_TODO,
		payload: todo_id
	};
};

export const setTodoLoadingAction = is_loading => {
	return {
		type: TODO_LOADING,
		payload: is_loading
	};
};
