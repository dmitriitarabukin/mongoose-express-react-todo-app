export const GET_ERRORS = "GET_ERRORS";
export const USER_LOADING = "USER_LOADING";
export const SET_CURRENT_USER = "SET_CURRENT_USER";
export const SET_TODOS = "SET_TODOS";
export const ADD_TODO = "ADD_TODO";
export const TODO_LOADING = "TODO_LOADING";
export const DELETE_TODO = "DELETE_TODO";
export const UPDATE_TODO = "UPDATE_TODO";