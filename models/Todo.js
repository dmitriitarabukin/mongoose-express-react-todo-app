const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const TodoSchema = new Schema({
	user: { type: Schema.Types.ObjectId, ref: "users" },
	text: String
});

module.exports = Todo = mongoose.model("todos", TodoSchema);
